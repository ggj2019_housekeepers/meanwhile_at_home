%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: HousekeeperLegs
  m_Mask: 00000000000000000000000001000000010000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/BroomRoot
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomBrush1
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomBrush1/BroomBruch2
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomBrush1/BroomBruch2/BroomBruch2_end
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomLeftIKTargt
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomLeftIKTargt/BroomLeftFingerIKTargt
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomLeftIKTargt/BroomLeftFingerIKTargt/BroomLeftFingerIKTargt_end
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomRightIKTargt
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomRightIKTargt/BroomRightFingerIKTargt
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomRightIKTargt/BroomRightFingerIKTargt/BroomRightFingerIKTargt_end
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomTop
    m_Weight: 0
  - m_Path: Armature/BroomRoot/BroomTop/BroomTop_end
    m_Weight: 0
  - m_Path: Armature/leg_L
    m_Weight: 1
  - m_Path: Armature/leg_L/leg_L_end
    m_Weight: 0
  - m_Path: Armature/leg_R
    m_Weight: 1
  - m_Path: Armature/leg_R/leg_R_end
    m_Weight: 0
  - m_Path: Armature/Root.003
    m_Weight: 0
  - m_Path: Armature/Root.003/Root.003_end
    m_Weight: 0
  - m_Path: Armature/Spine1
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Head
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Head/Head_end
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_L
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_L/UpperArm_L
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_L/UpperArm_L/ForeArm_L
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_L/UpperArm_L/ForeArm_L/Hand_L
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_L/UpperArm_L/ForeArm_L/Hand_L/Hand_L_end
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_R
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_R/UpperArm_R
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_R/UpperArm_R/ForeArm_R
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_R/UpperArm_R/ForeArm_R/Hand_R
    m_Weight: 0
  - m_Path: Armature/Spine1/Chest/Shoulder_R/UpperArm_R/ForeArm_R/Hand_R/Hand_R_end
    m_Weight: 0
  - m_Path: Broom
    m_Weight: 0
  - m_Path: HousekeeperBody
    m_Weight: 0
