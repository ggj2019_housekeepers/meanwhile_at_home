﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PlayerController : MonoBehaviour
{
    public GameObject targetPointIndigatorPrefab;

    private NavMeshAgent playerAgent;
    
    public Animator anim;

    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private LayerMask clickableMask;

    [SerializeField]
    private float cleaningRate = 1f;

    public float CleaningRate
    {
        get
        {
            return cleaningRate;
        }

        private set
        {
            cleaningRate = value;
        }
    }

    private void Awake()
    {
        playerAgent = GetComponent<NavMeshAgent>();        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (GameManager.Manager.IsOver)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500, clickableMask))
            {
                playerAgent.SetDestination(hit.point);
                if (targetPointIndigatorPrefab != null)
                {
                    Instantiate(targetPointIndigatorPrefab, hit.point, Quaternion.identity);
                }
            }
        }
        anim.SetFloat("Speed", playerAgent.velocity.magnitude);        
    }    
}
