﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterDisaster : Disaster
{
    public Transform pointAppearBehindWindow;
    public Transform pointEntranceInsideRoom;
    public List<Transform> targets = new List<Transform>();
    public GameObject dirtPrefab;

    public Animator anim;

    public float waitBehindWindow;
    private float currentWait;

    public int targetCount;

    [SerializeField]
    private float maxDirtTime;

    private float currentDirtTime;

    private bool targetVisited;

    private NavMeshAgent agent;

    private void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = false;
        anim = GetComponentInChildren<Animator>();
    }

    private void Start()
    {       
        GetTargets();
        targetCount = Random.Range(1, targets.Count+3);
        transform.position = pointAppearBehindWindow.position;
        StartCoroutine(WaitBehindWindow());
        ResetDirtTimer();
    }

    private void Update()
    {
        if (agent.enabled)
        {
            if (anim != null)
            {
                anim.SetFloat("Speed", agent.velocity.magnitude);
            }
            currentDirtTime -= 1 * Time.deltaTime;
            if (currentDirtTime <= 0.5f)
            {
                DropDirt();
                ResetDirtTimer();
            }

            if (agent.remainingDistance <= 1 && targetCount > 0)
            {         
                SetTarget();
                targetCount--;
            }
            else if (!targetVisited && targetCount <= 0)
            {
                targetVisited = true;
                agent.SetDestination(pointEntranceInsideRoom.position);
            }
        }
        if (targetVisited && targetCount <= 0)
        {
            if (Vector3.Distance(transform.position, pointEntranceInsideRoom.position) <= 0.5f)
            {
                DropDirt();
                Destroy(gameObject);
            }
        }
    }

    public override void CleanDisaster(float cleanRate)
    {
        base.CleanDisaster(cleanRate);
    }

    IEnumerator WaitBehindWindow()
    {
        yield return new WaitForSeconds(waitBehindWindow);
        transform.position = pointEntranceInsideRoom.position;
        agent.enabled = true;
        DropDirt();
        SetTarget();
    }

    void SetTarget()
    {
        if (agent != null)
        {
            Vector3 targetPosition = targets[Random.Range(0, targets.Count)].position;
            agent.SetDestination(targetPosition);            
        }
        DropDirt();
        ResetDirtTimer();
    }
    void ResetDirtTimer()
    {
        currentDirtTime = Random.Range(0.5f, maxDirtTime);
    }

    void DropDirt()
    {
        Instantiate(dirtPrefab, transform.position, Quaternion.identity);
    }

    protected virtual void GetTargets()
    {
        pointAppearBehindWindow = GameObject.FindGameObjectWithTag("ThiefAppear").transform;
        pointEntranceInsideRoom = GameObject.FindGameObjectWithTag("ThiefInside").transform;
        ThiefTarget[] findTargets = FindObjectsOfType<ThiefTarget>();

        for (int i = 0; i < findTargets.Length; i++)
        {
            targets.Add(findTargets[i].transform);
        }
    }
}
