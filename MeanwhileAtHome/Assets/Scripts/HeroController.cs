﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using TMPro;


public class HeroController : MonoBehaviour
{
    public GameObject heroPrefab;
    public Transform heroPosition;
    public float heroTimer;
    public PlayableDirector startDirector;
    public GameObject endDirector;


    public delegate void heroOut();

    private float currentTimer;

    private int dirtCount;
    private int disasterCount;

    [SerializeField]
    private string introSpeech;
    
    [SerializeField]
    private string failSpeech;

    [SerializeField]
    private string winSpeech;

    private bool heroGoesOut;

    public float CurrentTimer
    {
        get
        {
            return currentTimer;
        }        
    }

    // Start is called before the first frame update
    void Start()
    {
        //currentTimer = heroTimer;
        //heroPrefab.SetActive(true);
        //heroPrefab.transform.position = heroPosition.position;
        //UIManager.Manager.speechBubble.SetActive(true);
        // UIManager.Manager.heroSlider.gameObject.SetActive(false);
        endDirector.SetActive(false);
        startDirector.Play();
        UIManager.Manager.speechBubble.GetComponentInChildren<TextMeshPro>().text = introSpeech;
        StartCoroutine(Intro());
        
    }

    // Update is called once per frame
    void Update()
    {
        if (heroGoesOut)
        {
            heroPrefab.SetActive(false);
            UIManager.Manager.heroSlider.gameObject.SetActive(true);
            // use hero animation here instead
            currentTimer += 1 * Time.deltaTime;
            if (CurrentTimer >= heroTimer)
            {
                //heroPrefab.SetActive(true);
                //heroPrefab.transform.position = heroPosition.position;      
                endDirector.SetActive(true);
                endDirector.GetComponent<PlayableDirector>().Play();
                GameManager.Manager.HeroIsBack = true;
                AudioManager.Manager.StopMusic(0);
                StartCoroutine(DramaticPause());
                currentTimer = heroTimer;
            }
            UIManager.Manager.SetHeroSilder(CurrentTimer,heroTimer);
        }
    }

    public void HeroOut()
    {
        UIManager.Manager.speechBubble.SetActive(false);
        UIManager.Manager.cleanPopUp.SetActive(true);
        UIManager.Manager.AutoCloseMenu(UIManager.Manager.cleanPopUp, 2);
        heroGoesOut = true;
        AudioManager.Manager.PlayMusic(0, 0, true);
    }
    IEnumerator DramaticPause()
    {
        heroGoesOut = false;
        AudioManager.Manager.PlaySound(0, 0);
        yield return new WaitForSeconds(1.5f);
        EvaluateCleanliness();
    }
    IEnumerator Intro()
    {
        UIManager.Manager.speechBubble.SetActive(true);
        AudioManager.Manager.PlaySound(3, 0);
        yield return new WaitForSeconds((float)startDirector.duration);
        HeroOut();
    }

    public void EvaluateCleanliness()
    {        
        dirtCount = FindObjectsOfType<Dirt>().Length;
        // set disaster count as well
        StartCoroutine(EvaluateWait());
    }

    IEnumerator EvaluateWait()
    {
        yield return new WaitForSeconds(1f);
        UIManager.Manager.speechBubble.SetActive(true);
        if (dirtCount > 0)
        {            
            UIManager.Manager.speechBubble.GetComponentInChildren<TextMeshPro>().text = failSpeech;
            AudioManager.Manager.PlaySound(2, 0);
            yield return new WaitForSeconds(1f);
            GameManager.Manager.GameOver(false);
        }
        else
        {            
            UIManager.Manager.speechBubble.GetComponentInChildren<TextMeshPro>().text = winSpeech;
            AudioManager.Manager.PlaySound(1, 0);
            yield return new WaitForSeconds(1f);
            GameManager.Manager.GameOver(true);
        }
    }
}
