﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDisaster : Disaster
{
    public ParticleSystem fireParticles;
    public GameObject dirtSpawnAfterDeath;
    public float maxFireHealth;
    public float fireGrowRate = 5;
    private float currentFireHealth;

    private void Update()
    {
        if (GameManager.Manager.HeroIsBack)
        {
            return;
        }
        currentFireHealth += fireGrowRate * Time.deltaTime;
        GrowFire();
        if (currentFireHealth >= maxFireHealth)
        {
            currentFireHealth = maxFireHealth;
            DisasterBurnt();
            return;            
        }
        
    }

    public void GrowFire()
    {
        var em = fireParticles.emission;
        em.rateOverTime = new ParticleSystem.MinMaxCurve(currentFireHealth, currentFireHealth*1.5f);
    }

    public override void CleanDisaster(float cleanRate)
    {
        base.CleanDisaster(cleanRate);
       
    }

    private void DisasterBurnt()
    {
        GameObject burntDirt = Instantiate(dirtSpawnAfterDeath, transform.position, Quaternion.identity);
        burntDirt.GetComponent<Dirt>().MaxDirtiness = 5;
        Destroy(gameObject);
    }
}
