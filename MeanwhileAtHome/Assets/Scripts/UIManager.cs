﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager Manager { get; set; }

    public GameObject pauseMenu;
    public GameObject winScreen;
    public GameObject failScreen;
    public GameObject speechBubble;
    public GameObject cleanPopUp;
    public TextMeshPro speechTM;
    public Slider heroSlider;

    private void Awake()
    {
        if (Manager == null)
        {
            Manager = this;
        }
        else if (Manager != this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        pauseMenu.SetActive(false);
        winScreen.SetActive(false);
        failScreen.SetActive(false);
        speechTM = speechBubble.GetComponentInChildren<TextMeshPro>();
        heroSlider.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenMenu()
    {
        pauseMenu.SetActive(true);
    }

    public void CloseMenu()
    {
        pauseMenu.SetActive(false);
    }

    public void SetHeroSilder(float currentValue, float maxValue)
    {
        heroSlider.maxValue = maxValue;
        heroSlider.value = currentValue;
    }

    public void AutoCloseMenu(GameObject menuToClose, float timeToClose)
    {
        StartCoroutine(AutoClose(menuToClose,timeToClose));
    }

    IEnumerator AutoClose(GameObject menu, float timeToClose)
    {
        yield return new WaitForSeconds(timeToClose);
        menu.SetActive(false);
    }
}
