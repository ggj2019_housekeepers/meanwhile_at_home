﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanDirt : MonoBehaviour
{
    public PlayerController playerController;

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Dirt>() != null)
        {
            other.GetComponent<Dirt>().CleanDirt(playerController.CleaningRate);
            playerController.anim.SetBool("Mobbing", other.GetComponent<Dirt>().CurrentDirtiness <=0.1f ? false:true);
        }
        else if (other.GetComponent<Disaster>() != null)
        {
            other.GetComponent<Disaster>().CleanDisaster(playerController.CleaningRate);
            playerController.anim.SetBool("Mobbing", other.GetComponent<Disaster>().CurrentDisasterHealth <= 0.1f ? false : true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Dirt>() != null)
        {            
            playerController.anim.SetBool("Mobbing", false);
        }
        else if (other.GetComponent<Disaster>() != null)
        {
            playerController.anim.SetBool("Mobbing", false);
        }
    }
}
