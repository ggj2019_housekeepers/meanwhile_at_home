﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThiefTarget : MonoBehaviour
{
    public GameObject dirtPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dirt"))
        {
            Instantiate(dirtPrefab,other.gameObject.transform.position,Quaternion.identity);
//            Debug.Log("Thief dirt");
        }
    }
}
