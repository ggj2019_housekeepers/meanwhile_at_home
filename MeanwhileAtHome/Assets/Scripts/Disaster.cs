﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disaster : MonoBehaviour
{
    public float maxDisasterHealth;
    private float currentDisasterHealth;

    public float CurrentDisasterHealth
    {
        get
        {
            return currentDisasterHealth;
        }

        private set
        {
            currentDisasterHealth = value;
        }
    }

    private void OnEnable()
    {
        CurrentDisasterHealth = maxDisasterHealth;
    }
  
    public virtual void CleanDisaster(float cleanRate)
    {
        CurrentDisasterHealth -= cleanRate * Time.deltaTime;
        if (CurrentDisasterHealth <= 0)
        {
            CurrentDisasterHealth = 0;
            Destroy(gameObject);
        }        
    }
}
