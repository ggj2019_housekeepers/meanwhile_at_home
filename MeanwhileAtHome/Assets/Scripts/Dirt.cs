﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirt : MonoBehaviour
{
    [SerializeField]
    private float maxDirtiness;
    private float currentDirtiness;

    private Animator anim;

    public float MaxDirtiness
    {
        get
        {
            return maxDirtiness;
        }

        set
        {
            maxDirtiness = value;
        }
    }

    public float CurrentDirtiness
    {
        get
        {
            return currentDirtiness;
        }

        private set
        {
            currentDirtiness = value;
        }
    }

    private void OnEnable()
    {
        CurrentDirtiness = MaxDirtiness;
        anim = GetComponent<Animator>();
    }

    public void CleanDirt(float cleanRate)
    {
        CurrentDirtiness -= cleanRate * Time.deltaTime;
        if (CurrentDirtiness <= 0)
        {
            CurrentDirtiness = 0;
            Destroy(gameObject);
        }
        if (anim != null)
        {
            anim.SetFloat("Dirtiness", CurrentDirtiness);
        }
    }
}
