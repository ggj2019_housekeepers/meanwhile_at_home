﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Manager { get; set; }

    private void Awake()
    {
        if (Manager == null)
        {
            Manager = this;
        }
        else if (Manager != this)
        {
            Destroy(gameObject);
        }
    }

    public AudioClip[] sfxSounds;
    public AudioClip[] ambientSounds;

    public AudioSource[] sfxChannels;
    public AudioSource[] musicSoundChannels;    

    public void PlaySound(int soundIndex, int soundChannelIndex)
    {
        sfxChannels[soundChannelIndex].PlayOneShot(sfxSounds[soundIndex]);
    }

    public void PlayMusic(int musicIndex,int musicChannelIndex, bool loop)
    {
        musicSoundChannels[musicChannelIndex].clip = ambientSounds[musicIndex];
        musicSoundChannels[musicChannelIndex].loop = loop;
        musicSoundChannels[musicChannelIndex].Play();
    }

    public void StopMusic(int channel)
    {
        musicSoundChannels[channel].Stop();
    }
}
