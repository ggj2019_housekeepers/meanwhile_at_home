﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtSpawner : MonoBehaviour
{
    public GameObject[] dirtsToSpawn;

    public float spawnRate;

    private float currentTime;

    public Collider dirtAreaReference;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = spawnRate;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnDirt();
    }

    void SpawnDirt()
    {
        if (GameManager.Manager.HeroIsBack)
        {
            return;
        }

        currentTime -= 1 * Time.deltaTime;
        if (currentTime <= 0)
        {
            int randomDirtItem = Random.Range(0, dirtsToSpawn.Length);
            Instantiate(dirtsToSpawn[randomDirtItem], RandomisePosition(), Quaternion.identity);
            currentTime = spawnRate;            
        }
    }

    Vector3 RandomisePosition()
    {
        float minX = dirtAreaReference.bounds.min.x;
        float posX = Random.Range(minX, dirtAreaReference.bounds.max.x); 
        float minZ = dirtAreaReference.bounds.min.z;
        float posZ = Random.Range(minZ, dirtAreaReference.bounds.max.z);
        Vector3 resultPosition = new Vector3(posX,0,posZ);
        return resultPosition;
    }
}
