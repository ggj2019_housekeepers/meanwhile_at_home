﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Manager { get; set; }

    public bool IsOver
    {
        get
        {
            return isOver;
        }

        private set
        {
            isOver = value;
        }
    }

    public bool HeroIsBack { get; set; }

    public KeyCode pauseKey;
    private bool isOver;

    private void Awake()
    {
        if (Manager == null)
        {
            Manager = this;
        }else if (Manager != this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (Time.timeScale < 1)
        {
            Time.timeScale = 1;
        }
       
    }

    public void GameOver(bool isClear)
    {
        if (isClear)
        {
            Debug.Log("Level Clear!");
            UIManager.Manager.winScreen.SetActive(true);  
        }
        else
        {
            Debug.Log("Level Failed!");
            UIManager.Manager.failScreen.SetActive(true);
        }
        isOver = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(pauseKey) && SceneManager.GetActiveScene().buildIndex!=0)
        {
            Pause();
        }
    }

    public void Pause()
    {
        if (Time.timeScale < 1)
        {
            Time.timeScale = 1;
            UIManager.Manager.CloseMenu();
        }
        else
        {
            UIManager.Manager.OpenMenu();
            Time.timeScale = 0;
        }
    }

    public void SwitchLevel(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
